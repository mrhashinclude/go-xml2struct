package xml2struct

import (
	"io"

	"github.com/subchen/go-xmldom"
)

// XMLFileToStruct will convert the contents of the XML File parameter passed in into an untyped struct.
func XMLFileToStruct(file string) (map[string]interface{}, error) {
	dom, err := xmldom.ParseFile(file)
	if err != nil || dom == nil || dom.Root == nil {
		return nil, err
	}
	return XMLDomToStruct(dom.Root), nil
}

// XMLFhToStruct reads the XML from the file handle given, and converts it to an untyped struct.
func XMLFhToStruct(fh io.Reader) (map[string]interface{}, error) {
	dom, err := xmldom.Parse(fh)
	if err != nil || dom == nil || dom.Root == nil {
		return nil, err
	}
	return XMLDomToStruct(dom.Root), nil
}

// XMLDomToStruct converts the NOTAM XML into an untyped struct, (using map[string]interface{})
// which can subsequently be serialized as a JSON blob.
func XMLDomToStruct(node *xmldom.Node) map[string]interface{} {
	if node == nil {
		return nil
	}

	result := make(map[string]interface{})
	// Need this because result[key] is not a slice (even if it's ultimately going to hold one),
	// so append() won't work on it. multiResult is needed to accumulate the values (or objects)
	// of multiple tags of the same name into a single element of the resulting struct (or JSON,
	// if that's your end goal)
	multiResult := make(map[string][]interface{})

	if isLeaf(node) {
		key, value := leafValue(node)
		result[key] = value
		return result
	}
	instances, _ := childNameInstanceCount(node)

	for _, child := range node.Children {
		count := instances[child.Name]

		if count == 1 {
			// Create the object, with either the leaf value (if it's a leaf node), or a structure of whatever goes below it
			if isLeaf(child) {
				_, v := leafValue(child)
				result[child.Name] = v
			} else {
				result[child.Name] = XMLDomToStruct(child)
			}
		} else {
			if _, ok := multiResult[child.Name]; !ok {
				multiResult[child.Name] = make([]interface{}, 0)
			}
			if isLeaf(child) {
				_, v := leafValue(child)
				multiResult[child.Name] = append(multiResult[child.Name], v)
			} else {
				v := XMLDomToStruct(child)
				multiResult[child.Name] = append(multiResult[child.Name], v)
			}
		}
	}

	// Merge all the multi-value elements into the main result set
	for k, v := range multiResult {
		result[k] = v
	}
	return result
}

// Returns a map of [child name] => num_instances, and whether we found only unique children.
// This lets us build a more sensible JSON by flattening the multiple occurences into an array.
// e.g. <a> <b>...</b> <b>...</b> <c>...</c> </a> can become
// 		"a": { "b": [...], "c": {...} } rather than
//		"a": [ { "b": {...} }, { "b": {...} }, { "c": {...} } ]
func childNameInstanceCount(node *xmldom.Node) (counts map[string]int, uniqueChildren bool) {
	counts = make(map[string]int)
	uniqueChildren = true
	if node == nil {
		return
	}

	for _, child := range node.Children {
		name := child.Name
		if currentCount, ok := counts[name]; ok {
			counts[name] = currentCount + 1
			uniqueChildren = false
		} else {
			counts[name] = 1
		}
	}
	return
}

// Checks if a given node is a leaf node (i.e. no children)
func isLeaf(node *xmldom.Node) (leaf bool) {
	leaf = false
	if node.Children == nil || len(node.Children) == 0 {
		leaf = true
	}
	return
}

// Returns the (key, value) of a leaf node
func leafValue(node *xmldom.Node) (key string, value string) {
	key = node.Name
	value = node.Text
	return
}

package xml2struct

import (
	"testing"
)

func TestParseLocalFile(t *testing.T) {
	src := "./testdata/short.xml"
	_, e := XMLFileToStruct(src)
	if e != nil {
		t.Errorf("Unable to convert: %s", e)
	}
}
